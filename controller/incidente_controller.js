const db = require('../models/index');

class IncidenteController {

  crear(req, res) {
    var point = { type: 'Point', coordinates: [19.518289,-70.678392] };
    var data = {
      descripcion: req.body.descripcion,
      severidad: req.body.severidad,
      status: 'pendiente',
      patrullaId: parseInt(req.body.patrullaId, 10),
      ubicacion: point 
    };

    db.Incidente.create(data).then((incidente) => {
      res.send(incidente);
    })
    .catch((error) => {
      console.log(error);
    });

  }

  list(req, res) {
    db.Incidente.findAll().then(incidentes => {
        res.render('Dashboard/incidentes', {incidentes});
    });
  }

  visualizar(req, res) {
    const id = req.params.id;
    db.Incidente.findById(id).then(result =>
      res.render('Dashboard/verIncidente', {result})
    );
  }
  
}
  
  module.exports = IncidenteController;