const db = require('../models/index');
const modelo = require('../models/');

class PatrullaController {

  ver(req, res) {
    db.Patrulla.findAll({ include: [ 
      { 
        model: modelo.Incidente,
        limit: 5,
        order: [
          // Ordenar por DESC
          ['id', 'DESC']
        ]
      },{ 
        model: modelo.Medicion,
        limit: 1,
        order: [
          // Ordenar por DESC
          ['id', 'DESC']
        ],
        where: { 
          tipo: 'velocidad'
        }
      } 
    ], 
    where: { 
      id: req.params.id 
    } 
  }).then((patrulla) => {
      console.log(patrulla);
      res.render('Dashboard/verPatrulla', { patrulla });
    })
  };

  crear(req, res) {

    db.Patrulla.findOne({ where: { matricula: req.body.matricula } }).then(patrulla => {
      
      if (patrulla) {
        res.send( { message: 'Esta patrulla ya esta regristrada' } );
      } else {
        var point = { type: 'Point', coordinates: [39.807222,-76.984722] };
        var data = {
          matricula: req.body.matricula,
          placa: req.body.placa,
          marca: req.body.marca,
          modelo: req.body.modelo,
          ubicacion: point 
        };

        db.Patrulla.create(data).then((patrulla) => {
          res.send(patrulla);
        })
        .catch((error) => {
          console.log(error);
        });
  
      }
    });

  }

  list(req, res) {
    db.Patrulla.findAll().then(patrullas => {
        res.render('Dashboard/patrullas', {patrullas});
    });
  }

}

module.exports = PatrullaController;