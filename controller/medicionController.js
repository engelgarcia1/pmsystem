const db = require('../models/index');
const modelo = require('../models/');

class MedicionController {

    crear(req, res) {
        var data = {
            tipo: req.body.tipo,
            medicion: req.body.medicion,
            patrullaId: parseInt(req.body.patrullaId, 10)
        };

        db.Medicion.create(data).then((medicion) => {
            res.send(medicion);
        })
        .catch((error) => {
            console.log(error);
        });
    }

}

module.exports = MedicionController;