const db = require('../models/index');
var jwt = require('jwt-simple');
const User = require('../models/user');
const MobileController = require('../mobile/controllers');
const mobile = new MobileController();

class UserController {

  list(req, res) {
    db.user.findAll().then(user => {
        res.render('Dashboard/users', {user});
    });
  }

  gprsTest(req, res) {
    db.user.findAll().then(user => {
        res.send({user});
    });
  }

  // Mobile controller
  
  // Todos los metodos para el movil van aqui
  authenticate(req, res) {
    db.user.findOne({
      where: {
        email: req.body.email
      }
    }).then(user => {
      if(!user) {
        res.status(403).send({success: false, msg: 'No existe usuario con el correo proporcionado' });
      } else {
        mobile.comparePassword(req.body.password, user.password, function(err, isMatch){
          if(isMatch && !err) {
            var token = jwt.encode(user, 'rHUyjs6RmVOD06OdOTsVAyUUCxVXaWci')
            res.send({ success: true, token: token })
          } else {
            return res.status(403).send({success: false, msg: 'La clave es incorrecta' })
          }
        })
      }

    });
  }

  getInfo(req, res) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        var token = req.headers.authorization.split(' ')[1]
        var decodedtoken = jwt.decode(token, 'rHUyjs6RmVOD06OdOTsVAyUUCxVXaWci')
        return res.json({success: true, msg: 'Hello ' + decodedtoken.firstname})
    }
    else {
        return res.json({success: false, msg: 'No Headers'})
    }
  }

}

module.exports = UserController;