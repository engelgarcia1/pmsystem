// Controlador de Usuario
const UserController = require('../controller/user_controller');
const usuario = new UserController();

// Controlador de Incidente
const IncidenteController = require('../controller/incidente_controller');
const incidente = new IncidenteController();

// Controlador de Patrulla
const PatrullaController = require('../controller/patrullaController');
const patrulla = new PatrullaController();

// Controlador de Medicion
const MedicionController = require('../controller/medicionController');
const medicion = new MedicionController();

// Controlador para Ubicacion
const UbicacionController = require('../controller/ubicacionController');
const ubicacion = new UbicacionController();

module.exports = (app, passport) => {

  app.get('/', isLoggedIn, (req, res) => {
    res.render('Dashboard/dashboard');
  });

  // ******* Rutas para la usuario ********* //
  
  app.get("/usuarios", isLoggedIn, (req, res) => usuario.list(req, res));

  
  // ******* Rutas para la API ********* //
  
  app.get("/api/gprs", (req, res) => usuario.gprsTest(req, res));

  // Crear Ubicacion
  app.get("/api/crearUbicacion", (req, res) => ubicacion.crear(req, res));
  
  
  // ******** Rutas para Incidentes ********* //

  app.post("/incidentes/crear", (req, res) => incidente.crear(req, res));

  app.get("/incidentes", isLoggedIn, (req, res) => incidente.list(req, res));

  app.get("/incidentes/ver/:id", (req, res) => incidente.visualizar(req, res));

  app.get('/incidentes/crear', isLoggedIn, (req, res) => {
    res.render('Dashboard/create_incidente');
  });

  // ****** Rutas para patrulla ******** ///

  app.post("/patrulla/crear", (req, res) => patrulla.crear(req, res));

  // Rutas para Incidentes
  app.get("/patrulla", isLoggedIn, (req, res) => patrulla.list(req, res));

  app.get("/patrulla/ver/:id", (req, res) => patrulla.ver(req, res));

  app.get('/patrulla/crear', isLoggedIn, (req, res) => {
    res.render('Dashboard/createPatrulla');
  });

  // Rutas para oficiales
  app.get('/oficiales', isLoggedIn, (req, res) => {
    res.render('Dashboard/oficiales');
  });

  // Rutas para mediciones
  app.post("/medicion/crear", (req, res) => medicion.crear(req, res));

  // Rutas para chat
  app.get('/chat', isLoggedIn, (req, res) => {
    res.render('Dashboard/chat');
  });

  // Logic for auth
  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next();
    res.redirect('/login');
  }
};