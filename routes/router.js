module.exports = (app, passport) => {
    app.get('/signup', (req, res) => {
      res.render('Auth/signup', { mensaje: req.flash('error')});
    });
  
    app.get('/login', (req, res) => {
      res.render('Auth/login', { mensaje: req.flash('error')});
    });
  
    app.post(
      '/signup',
      passport.authenticate('local-signup', {
        successRedirect: '/',
        failureRedirect: '/signup',
        failureFlash : true
      })
    );

    app.post(
      '/login',
      passport.authenticate('local-signin', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash : true
      })
    );

    app.get('/logout', (req, res) => {
      req.session.destroy(err => {
        res.redirect('/');
      });
    });
  
    function isLoggedIn(req, res, next) {
      if (req.isAuthenticated()) return next();
  
      res.redirect('/login');
    }
  };
  