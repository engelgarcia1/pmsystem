const Incidente = require('../models/incidente');

module.exports = (sequelize, Sequelize) => {
  var Patrulla = sequelize.define('Patrulla', {
    id: { 
      autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
    },
    matricula: {
      type: Sequelize.STRING
    },
    placa: {
      type: Sequelize.STRING
    },
    marca: {
      type: Sequelize.STRING
    },
    modelo: {
      type: Sequelize.STRING
    },
    hub: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.ENUM('activo', 'inactivo', 'taller'),
      defaultValue: 'activo'
    }
  });

  return Patrulla;
};