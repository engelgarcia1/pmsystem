module.exports = (sequelize, Sequelize) => {
    var Zona = sequelize.define('Zona', {
      id: { 
        autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
      },
      ubicacion: {
        type: Sequelize.GEOMETRY('POINT', 4326)
      }
    });
  
    return Zona;
};