module.exports = (sequelize, Sequelize) => {
    var Destacamento = sequelize.define('Destacamento', {
      id: { 
        autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
      },
      nombre: {
        type: Sequelize.STRING
      }
    });
  
    return Destacamento;
};