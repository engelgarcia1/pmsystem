module.exports = (sequelize, Sequelize) => {
    var Falla = sequelize.define('Falla', {
      id: { 
        autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
      },
      codigo: {
        type: Sequelize.STRING
      }
    });
  
    return Falla;
};