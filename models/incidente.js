const Patrulla = require('../models/patrulla');

module.exports = (sequelize, Sequelize) => {
  var Incidente = sequelize.define('Incidente', {
    id: { 
      autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
    },
    tipo: {
      type: Sequelize.STRING
    },
    detalles: {
      type: Sequelize.STRING
    },
    severidad: {
      type: Sequelize.STRING
    },
    estado: { 
      type: Sequelize.ENUM('pendiente', 'inactivo', 'cancelado'),
      defaultValue: 'pendiente'
    },
    perInvolucradas: {
      type: Sequelize.INTEGER
    },
    nombreContacto: {
      type: Sequelize.STRING
    },
    numeroContacto: {
      type: Sequelize.STRING
    }
  });

  return Incidente;

};