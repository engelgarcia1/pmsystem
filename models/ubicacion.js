module.exports = (sequelize, Sequelize) => {
    var Ubicacion = sequelize.define('Ubicacion', {
      id: { 
        autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
      },
      ubicacion: {
        type: Sequelize.GEOMETRY('POINT', 4326)
      },
      longitud: {
        type: Sequelize.DECIMAL
      },
      latitud: {
        type: Sequelize.DECIMAL
      },
      sector: {
          type: Sequelize.STRING
      },
      municipio: {
          type: Sequelize.STRING
      }
    });
  
    return Ubicacion;
};