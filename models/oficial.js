module.exports = (sequelize, Sequelize) => {
    var Oficial = sequelize.define('Oficial', {
      id: { 
        autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
      },
      nombre: {
        type: Sequelize.STRING
      },
      apellido: {
        type: Sequelize.STRING
      },
      rango: {
        type: Sequelize.STRING
      },
      cedula: {
        type: Sequelize.STRING
      },
      telefono: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      }
    });
  
    return Oficial;
  
  };