module.exports = (sequelize, Sequelize) => {
  var Medicion = sequelize.define('Medicion', {
    id: { 
      autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER 
    },
    tipo: {
      type: Sequelize.STRING
    },
    medicion: {
      type: Sequelize.STRING
    }
  });

  return Medicion;

};