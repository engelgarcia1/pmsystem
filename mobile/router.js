// Controlador de Usuario
const UserController = require('../controller/user_controller');
const usuario = new UserController();

module.exports = (app) => {
    app.post("/api/auth", usuario.authenticate)
    app.get("/api/getinfo", usuario.getInfo)
}