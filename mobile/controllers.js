//load bcrypt
const bCrypt = require('bcrypt-nodejs');

class MobileController {

    comparePassword(password, password2, res) {
        bCrypt.compare(password, password2, function(err, isMatch){
          if(err) {
            return res(err);
          } else {
            res(null, isMatch);
          }
        });
    }

}

module.exports = MobileController