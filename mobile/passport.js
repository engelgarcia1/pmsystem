var JwtStrategy = require('passport-jwt').Strategy
var ExtractJwt = require('passport-jwt').ExtractJwt

var User = require('../models/user')
var config = require('../config/config.json')

module.exports = function (passportMobile) {
    var opts = {}

    opts.secretOrKey = 'rHUyjs6RmVOD06OdOTsVAyUUCxVXaWci'
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt')

    passportMobile.use(new JwtStrategy(opts, function (jwt_payload, done) {
        User.find({
            id: jwt_payload.id
        }, function (err, user) {
                if (err) {
                    return done(err, false)
                }
                if (user) {
                    return done(null, user)
                }

                else {
                    return done(null, false)
                }
        }
        )
    }))
}